package com.example.helloconsumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Consumer {

    @RabbitListener(queues = "helloQ")
    public void receiveMessage(String message){
        log.info("message >>> " + message);
    }
}
